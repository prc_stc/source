#include "../../includes.h"


/*
 *
 * 1 -> Create thread routine[static void *threadRoutine(void *arg)] the void *arg is argument which is passed from the main function
 *      to convert void *arg to whatever type use cast. In thread routine function to return a result you need to call 
 *      pthread_exit((void * )status) where status in this case is amount of number from file.
 * 2 -> to alloc space for threads use pthread_t <variable> = (pthread_t *)malloc(<int variable> * sizeof(pthread_t))
 * 3 -> to retrieve to status of each thread call pthread_join(pthread_t thread, void **status);
 */

#define MAX_READ 1

/* thread Routine to calculate the number apparitions from file which is given as argument */

static void *threadRoutine(void *arg)
{
	/* get the filename */
	char *fileName = (char *) arg;  
	int fd;
	
	/*open file for read only */
	fd = open(fileName, O_RDONLY);
	
	if(fd == -1)
	{
		if(errno = ENOENT)
		{
			printf("File not found\n");
		} else 
		{
			printf("Error open file");
		}
	}
	
	int nRead;
	int cNumber = 0;
	char buffer[MAX_READ];
	
	/*
	 * 
	 * The whole point to calculate the number apparitions on file is read a carcater by caracter
	 * and test if is digit(isdigit() functions from ctype.h) , if is digit then increment the 
	 * value cNumber
	 *
	 */
	 
	while(nRead = read(fd, buffer, 1) > 0)
	{
		
		if(isdigit(buffer[0]))
		{
			++cNumber;
		}
	}
	
	if(nRead == -1)
	{
		printf("Error read file\n");
		exit(1);
	}
	
	/* result which is returned by each thread */
	pthread_exit((void *) cNumber);
	
}
int main(int argc , char *argv[])
{

	pthread_t *thread;
	int i;
	int j;
	void *status;
	int result;
	int total = 0;
	
	
	if(argc < 2 )
	{
		printf("Usage:%s <file/s>\n", argv[0]);
		exit(1);
	}
	/* Number of threads is equal to number of argument - 1 (argv[0] is program name) */
	int nThreads = argc;
	
	/* Alocate space for threads */
	thread = (pthread_t *)malloc(nThreads * sizeof(pthread_t));
	
	
	/* Create and execute each thread and passing argument to threads(filename)*/
	for(i = 1; i < nThreads; ++i)
	{
		int t = pthread_create(&thread[i], NULL, threadRoutine, argv[i]);
		
		if(t != 0)
		{
			printf("pthread_create error\n");
			exit(1);
		}
	}
	
	for(j = 1; j < nThreads; ++j)
	{
		int n = pthread_join(thread[j], &status);
		
		if(n)
		{
			printf("Error return code from pthread_join()\n");
			exit(1);
		}
		
		result = (int) status;
		total += result;
	}
	
	printf("Total Number:%d\n", total);
	free(thread);
	
	pthread_exit(NULL);

}

