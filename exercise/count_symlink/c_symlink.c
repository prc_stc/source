/*
 * 
 * Sa se scrie folosind functii unix si apeluri de sistem un program C 
 * care numara toate link-urile din subarborele unui director dat in linia de comanda.
 *
 */

#include "../../includes.h"

int main(int argc , char *argv[])
{
	if(argc != 2 )
	{
		printf("Usage: %s </path/to/dir> \n", argv[0]);
		exit(130);
	}
	
	DIR *o_dir; 
	struct dirent *dirptr;
	struct stat fileBuffer;
	struct stat dirBuffer;
	char *fileName;
	char *filePath;
	int lengthFileName = 0;
	int lengthFilePath = 0;
	int contor = 0;
	
	stat(argv[1], &dirBuffer);
	
	if(S_ISDIR(dirBuffer.st_mode))
	{
		
	// Open directory using function DIR *opendir(const char *pathname);
	// Succes return ptr to DIR else return NULL
	
		o_dir = opendir(argv[1]);
	
		if(o_dir == NULL)
		{
			printf("ERROR: opendir for %s \n", argv[1]);
			exit(131);
		}
	
	// To read each entry form directory we using function struct dirent *readdir(DIR *ptr)
	
		while((dirptr = readdir(o_dir)) != NULL)
		{
			lengthFileName = strlen(dirptr->d_name);
			lengthFilePath = strlen(argv[1]) + lengthFileName;
		
			if((fileName = malloc(lengthFileName * sizeof(char))) == NULL)
			{
				printf("ERROR: malloc () on %s\n", dirptr->d_name);
				exit(132);
			}
		
			if((filePath = malloc((2 * lengthFilePath) * sizeof(char))) == NULL)
			{
				printf("ERROR: malloc() on %s\n", "filePath");
				exit(133);
			}
		
			// save  directory name
			
			strcpy(filePath, argv[1]);
			
			//In case if /path/to/dir not contain "/ at the end of argument"
			int len = strlen(argv[1]);
			
			char *slash = &argv[1][len - 1];
			
			if(strcmp(slash, "/") != 0)
			{
				strcat(filePath, "/");
			} 
				strcpy(fileName, dirptr->d_name);
				strcat(filePath, fileName);
				
				if(lstat(filePath, &fileBuffer) == -1)
				{
					printf("ERROR: lstat() \n");
					exit(135);
				}
			
				if(S_ISLNK(fileBuffer.st_mode))
				{
					contor++;
				}
			
		}
	} else
	{
		printf("%s is not a directory\n", argv[1]);
		printf("Usage: %s </path/to/directory\n", argv[0]);
		exit(134);
	}
	
	printf("%s has %d symlink files \n", argv[1], contor);
	
	return 0;
				
}
