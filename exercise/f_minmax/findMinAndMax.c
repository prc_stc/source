/*
 * 
 * Scrieti un program care citeste dintr-un fisier linii continand cate 50 de numere intregi . 
 * Pentru fiecare linie, intr-un thread separat, programul determina minimul si maximul. 
 * In final, programul va determina cel mai mare minim si cel mai mic maxim si le va afisa impreuna cu numarul liniei pe care au aparut.
 * *                                                         *
 *  In C arrays are passed as a pointer to the first element *
 * *                                                         * 
 */


#include "../../includes.h"

#define MAX_NUM 5
#define MAX_RETURN 3

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

int array[MAX_NUM];

struct info 
{
	int min;
	int max;
	int line;
}my_info;

static void *threadRoutine(void *arg)
{
	int i;
	int j;
	int mn = 0;
	int mx = 0;
	int ln = 0;
	int *index = (int *) arg;
	
	 struct info *result = &my_info;
	
	++ln;
	mn = index[0];
	for(i = 0; i < MAX_NUM; ++i)
	{
		mn = MIN(index[i], mn);
		mx = MAX(index[i], mx);
	}
	
	result->min = mn;
	result->max = mx;
	result->line = ln;
	
	pthread_exit((void *)result);	
	
}

int main(int argc, char *argv[])
{
	if(argc != 2)
	{
		printf("Usage: %s <file> \n", argv[0]);
		exit(-1);
	}
	
	int fd;
	
	fd = open(argv[1], O_RDONLY);
	
	if(fd == -1)
	{
		if(errno == ENOENT)
		{
			printf("File don't exist\n");
			exit(-1);
		}else
		{
			printf("Unable to open %s \n", argv[1]);
			exit(-1);
		}
	}
	
	int nRead;
	int i = 0;
	int t;
	char *buffer;
	void * status;
	pthread_t *thread;
	
	if((buffer = malloc(2* sizeof(char))) == NULL)
	{
		printf("ERROR: malloc() -> nRead\n");
		exit(-1);
	}
	
	
	while((nRead = read(fd, buffer, 2)) > 0)
	{	
		array[i] = atoi(buffer);
		++i;
	}
	
	thread = (pthread_t *)malloc(1 * sizeof(pthread_t));
	
	t = pthread_create(thread, NULL, threadRoutine, &array);
	
	if(t != 0)
	{
		printf("ERROR: pthread_create\n");
		exit(-1);
	}
	
	if(pthread_join(*thread, &status) != 0)
	{
		printf("ERROR: pthread_join \n");
		exit(-1);
	}
	
	struct info *res = (int *)status;
	
	printf("MIN %d\nMAX %d\nLINE %d\n\n", res->min, res->max, res->line);
	
	
	free(buffer);
	free(thread);
	
	pthread_exit(NULL);		
	
}
