/*
 *
 * Folosind apeluri sistem si functii de biblioteca C pt lucrul cu fisiere si directoare in UNIX, sa se scrie un program care, primind doi parametrii in linia
 * de comanda (un nume de cale si un sir de caractere),cauta si afiseaza numele tuturor fisierelor din subarborele caii respective care au in continutul lor
 * sirul de caractere dat.
 *
 */

#include "../../home/andr3w/workspace/labs/c/includes.h"

int main(int argc, char *argv[])
{
	if(argc != 3)
	{
		printf("Usage:%s <path> <string pattern>\n", argv[0]);
		exit(1);
	}
	
	DIR *dirptr; // to use opendir()
	struct dirent *dirReadPtr;
	struct stat buffer;
	
	char *fileName;
	char *filePath;
	char *pattern;
	

	dirptr = opendir(argv[1]);

	if(dirptr == NULL)
	{
		printf("ERROR -> opendir failed on %s \n", argv[1]);
		exit(-1);
	}

	while((dirReadPtr = readdir(dirptr)) != NULL)
	{
		
		int length = strlen(argv[1]) + strlen(dirReadPtr->d_name);
		
		if((fileName = malloc(strlen(dirReadPtr->d_name) * sizeof(char))) == NULL)
		{
			printf("ERROR: malloc() to fileName\n");
			exit(-1);
		}
		
		if((filePath = malloc((10 * length) * sizeof(char))) == NULL)
		{
			printf("ERROR: malloc() to filePath\n");
			exit(-1);
		}
		
		strcpy(filePath, argv[1]);
		strcpy(fileName, dirReadPtr->d_name);
		
		strcat(filePath, "/");
		strcat(filePath, fileName);
		
		stat(filePath, &buffer);
		
		if(S_ISREG(buffer.st_mode))
		{
		
			if((pattern = malloc(strlen(argv[2]) * sizeof(char))) == NULL)
			{
				printf("ERROR: malloc() pattern\n");
				exit(-1);
			}
			
			int fd;
			int nRead;
			
			if((fd = open(filePath, O_RDONLY)) < 0 )
			{
				printf("ERROR: open() on %s\n", filePath);
				exit(-1);
			}
			while((nRead = read(fd, pattern, strlen(argv[2]))) > 0)
			{
				if(strcmp(pattern, argv[2]) == 0)
				{
					printf("%s\n", filePath);
				}
			}
		}
		
		
	}
	
	
	
	return 0;

}
